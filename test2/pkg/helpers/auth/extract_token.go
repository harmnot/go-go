package auth

import (
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"os"
)

func ExractToken(jwttoken string) (token *jwt.Token, err error) {
	getScret := os.Getenv("JWT_SECRET")
	token, err = jwt.Parse(jwttoken, func(jwttoken *jwt.Token) (interface{}, error) {
		if _, ok := jwttoken.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method")
		}
		return []byte(getScret), nil
	})
	return
}
