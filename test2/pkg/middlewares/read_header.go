package middlewares

import (
	"api/pkg/helpers/auth"
	"database/sql"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func ReadTokenHeader(db *sql.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		getToken := c.GetHeader("token")
		token, _ := auth.ExractToken(getToken)
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			var iD float64
			var userId int

			switch theId := claims["id"].(type) {
			case float64:
				iD = theId
			}
			_ = db.QueryRow("SELECT id FROM users WHERE id = $1", iD).Scan(&userId)
				if userId < 1 {
					c.AbortWithStatusJSON(404, gin.H{
						"error": "Not Authorize",
					})
					return
				}
				c.Next()
		} else {
			c.AbortWithStatusJSON(400, gin.H{
				"error": "token expired, please login again",
			})
			return
		}
	}
}
