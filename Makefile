testOne:
	@echo "RUNNING Test 1...\n"
	@echo "\nPROBLEM 1"
	go run test1/problem_1.go
	@echo "\nPROBLEM 2"
	go run test1/problem_2.go
	@echo "\nPROBLEM 3"
	go run test1/problem_3.go
	@echo "\nPROBLEM 4"
	go run test1/problem_4.go
	@echo "\nEND Test 1..."