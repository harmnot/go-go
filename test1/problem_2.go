package main

import "fmt"

func sort(arr []int) []int {
	for i := 0; i < len(arr); i++ {
		for j := i+1; j < len(arr); j++ {
			if arr[i]> arr[j] {
				temp := arr[i]
				arr[i] = arr[j]
				arr[j] = temp
			}
		}
	}
	return arr
}

func main() {
	resultOne, resultTwo := sort([]int{0,1,2,2,1,0,0,2,0,1,1,0}), sort([]int{10,10,2,2,1,13,0,2,0,1,1,0})
	fmt.Println(resultOne)
	fmt.Println(resultTwo)
}
