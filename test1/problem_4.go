package main

import (
	"fmt"
	"math"
)

func SubArray2(arr []float64) string {
	var (
		maxTemp float64
		minTemp float64
		currentMax float64
	)

	maxTemp = 1
	minTemp = 1
	currentMax = 1

	for _, v := range arr {
		val := v

		if val < 0 {
			temp := maxTemp
			maxTemp = minTemp
			minTemp = temp
		}

		maxTemp = math.Max(val,  maxTemp * val)
		minTemp = math.Min(val, minTemp * val)
		currentMax = math.Max(maxTemp, currentMax)
	}

	return fmt.Sprintf(" having product %v", currentMax)
}

func main() {
	resultOne, resultTwo := SubArray2([]float64{-6,4,-5,8,-10,0,8}), SubArray2([]float64{-6,4,-5,10,-10,0,8})
	fmt.Println(resultOne)
	fmt.Println(resultTwo)
}
