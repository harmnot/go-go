package main

import "fmt"

func findDup(arr []int) string {
	tempMap, result := make(map[int][]int), make([]int, 0)
	for _, v := range arr {
		tempMap[v] = append(tempMap[v], v)
		if len(tempMap[v]) >= 2 {
			result = append(result, v)
		}
	}
	return fmt.Sprintf("the duplicate element is %d", result[0])
}

func main() {
	resultOne, resultTwo := findDup([]int{1, 2, 3, 4, 2, 3}), findDup([]int{1, 2, 3, 4, 4})
	fmt.Println(resultOne)
	fmt.Println(resultTwo)
}
