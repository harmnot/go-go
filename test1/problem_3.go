package main

import "fmt"

func Combine(arr []int, length int) (final [][]int) {
	var Repeat func(index int, right []int)
	Repeat = func(index int, right []int)  {
		if len(right) == length {
			final = append(final, right)
			return
		}
		for index < len(arr) {
			concatRight := append(right, arr[index])
			Repeat(index, concatRight)
			index++
		}
	}
	var emptyList []int
	Repeat(0, emptyList )
	return final
}

func main() {
	arr := []int{1, 2, 3}
	final := Combine(arr, 2)
	fmt.Println(final)
}
