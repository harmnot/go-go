## How to use this app

>>
>NOTE
>> 1. this app using Go 1.14.3, if you don't have Go in your machine
>please install, for [linux](https://www.linode.com/docs/development/go/install-go-on-ubuntu/) and for [mac](https://medium.com/golang-learn/quick-go-setup-guide-on-mac-os-x-956b327222b8)
>> 2. this app also using Makefile, please make sure you have it in your machine,
>if you can't install or use Makefile, just copy-paste commands in the file on Makefiles on this app
>> 3. make sure has latest Postgres (12.3)
>>
>check packages here: [install all os packages](https://command-not-found.com/?)

### test 1

this **test 1** has 4 files, if you want run all files just run:
```javascript
local~$ make testOne
```
if you want to run for single file, check into Makefile and copy-paste the command inside there
:)


### test 2

this API is about Quote that user be able to get random quotes
from 3rd party API and then you can favorites them into your accounts
(required create account)
this API would be able to make you understanding:
- using jwt for authentication
- CRUD data to database without ORM 
- unit testing
- using sql in go for migration and mock data

let's start for run the app:

before we begin make sure that you already read notes above before to ahead,
first thing we need to create user and database name:
```javascript
local~$ cd test2/scripts
local~$ make setup
```
>>
>this commands created database & user for local/dev and test

then you need to create tables, if you are a Ruby dev before, you might know
how quick to create database and migration with single command, we do can do this also in
Go, just run migration by:
* check before migrate
```javascript
local~$ make db_test_dev
```
* if the command above has no error, apply the migration:
```javascript
local~$ make db_migrate
local~$ make db_migrate_test
```
* if you want to undo migration for dev/local
```javascript
local~$ make db_undo
```
i created a seed file also to seed one user into your local and test database
```javascript
local~$ make db_seed
local~$ make db_seed_test
```

if you want to create new migration you can run:
```javascript
local~$ export DB_DEV_USERNAME=testonly DB_DEV_PASSWORD=test DB_DEV_NAME=logic && \
                sql-migrate new -config=../configs/dbconfig.yml -env="development" create_something_table
```
and then check into `configs/migrations`

before you run the app make sure you test the expected this app, to run 
**unit test**:
```javascript
local~$ make test
```

ok, after they are `PASS` then you can run the main app by locally:
```javascript
local~$ make dev
```

on your terminal you can see all routes over there and you can run them into 
**POSTMAN**

for to ahead how to use the endpoints `localhost:3000`, read this carefully:

| endpoint               | method | header                          | request                                                                                                                                                                                                                       | response success                                                                                                                                                   | response failed                                |
|------------------------|--------|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------|
| /v1/quotes             | GET    | -                               | -                                                                                                                                                                                                                             | {  <br>   "en": QUOTES,<br>   "author": AUTHOR_NAME,<br>   "id": ID<br>}                                                                                           | 400<br><br>{<br> "error": ERROR<br>}           |
| /v1/userquotes         | GET    | -                               | -                                                                                                                                                                                                                             | [{<br>    "id_of_quote": ID,<br>    "user_name": REGISTERED_USER,<br>    "quote": QUOTE,<br>    "author_quote": AUTHOR,<br>    "quote_created": DATE_CREATED<br>}] | 400<br><br>{<br> "error": ERROR<br>}           |
| /v1/user/quotes        | GET    | "token": YOUR_TOKEN_AFTER_LOGIN | -                                                                                                                                                                                                                             | [{ <br>    "id": ID,<br>    "quote": QUOTE,<br>    "author": AUTHOR,<br>    "created_at": DATE_CREATED<br>}]                                                       | 400, 407, 500<br><br>{<br> "error": ERROR<br>} |
| /v1/user/login         | POST   | -                               | {<br> "email": "test@email.com",<br> "password": "12345678"<br>}                                                                                                                                                              | {<br>  "token": TOKEN_FOR_LOGIN<br>}                                                                                                                               | 400,404<br><br>{<br> "error": ERROR<br>}       |
| /v1/user/create        | POST   | -                               | {<br> "name": "Test 2",<br> "email": "test2@email.com",<br> "password": "12345678"<br>}                                                                                                                                       | bool                                                                                                                                                               | 400,<br><br>{<br> "error": ERROR<br>}          |
| /v1/favoritequotes/:id | POST   | "token": YOUR_TOKEN_AFTER_LOGIN | - hit /v1/quotes to get randome quotes<br>- copy that "id" response<br>- hit /v1/favoritequotes/THAT_ID<br><br>EX: <br><br><br>/hit/favoritesquotes/5a9b0bb22bad9600044b6fcd<br>/hit/favoritesquotes/5a9802cb1878b40004879f59 | bool                                                                                                                                                               | 400, 407,<br><br>{<br> "error": ERROR<br>}     |
| /v1/deletequote/:id    | DELETE | "token": YOUR_TOKEN_AFTER_LOGIN | - hit /v1/user/quotes to get your favorites quotes<br>- copy that "id" response<br>- hit /v1/deletequote/THAT_ID<br><br>EX:<br><br>/v1/deletequote/1<br>/v1/deletequote/1,2<br>/v1/deletequote/2,3,4                          | bool                                                                                                                                                               | 400, 407<br><br>{<br> "error": ERROR<br>}      |